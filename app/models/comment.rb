class Comment < ActiveRecord::Base
  belongs_to :post
  validates_presence_of :post_id # validaando o post
  validates_presence_of :body # need a body
end
