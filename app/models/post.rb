class Post < ActiveRecord::Base
  # destroi comentario quando apagar o post
  has_many :comments, dependent: :destroy 
  
  # validations
  validates_presence_of :title
  validates_presence_of :body 
end
